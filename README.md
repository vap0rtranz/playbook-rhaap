# playbook-rhaap

Playbooks to setup Red Hat Ansible Automation Platform 2.0 and later.

## Documentation

More details can be found on the [Ansible Labs](https://www.ansible-labs.de) web site.

## Cloud provider environment variables

Make sure to provide your cloud provider access details as environment variables. For convenience, save it as a shell script which can be sourced when needed.

```bash
# Amazon AWS
export AWS_ACCESS_KEY_ID=''
export AWS_SECRET_ACCESS_KEY=''
# Azure
export AZURE_SECRET=
export AZURE_CLIENT_ID=
export AZURE_SUBSCRIPTION_ID=
export AZURE_TENANT=
# Google
export GCP_AUTH_KIND=serviceaccount
export GCP_SERVICE_ACCOUNT_FILE=~/.gcp.json
export GCP_PROJECT=myproject
# Automation Controller
export CONTROLLER_HOST=
export CONTROLLER_USERNAME=admin
export CONTROLLER_PASSWORD=
# RHV
export OVIRT_HOSTNAME=rhevm.ug.bolzchristian.de
export OVIRT_USERNAME=admin@internal
export OVIRT_PASSWORD=H1mSB34r1213..
```

it's only required to provide the credentials for your target provider. For example, if you plan to deploy on AWS, Azure and Google can be ignored and not defined.

## Variables

The minimum set of extra variables you have to define is documented in the [Setup extra variables](https://www.ansible-labs.de/installation/03-extra-variables/) section.

## Inventory

Since the `controller-instance` playbook is creating the cloud instance and adds it to the in-memory inventory, no inventory file is needed. You can use `/etc/ansible/hosts` if you have to provide an inventory file.

## Download Ansible Setup

Since Red Hat Ansible Automation Platform 2.0 can only be downloaded from [access.redhat.com](https://access.redhat.com) when you have a valid Subscription, you have to download the file manually and save it in the [roles/rhaap/files](roles/rhaap/files) folder. If you don't have a valid subscription, you can request a [free trial](https://www.redhat.com/en/technologies/management/ansible/try-it).

## Run the Playbook

It's recommended to use [Ansible Navigator](https://github.com/ansible/ansible-navigator/) with the [ee-ansible-ssa](https://gitlab.com/redhat-cop/ansible-ssa/ee-ansible-ssa) execution environment. Install Ansible navigator from the [Red Hat Repositories](https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.0-ea/html-single/ansible_navigator_creator_guide/index?lb_target=production) or by using pip (e.g. if you're on Fedora).

For your convenience, create an `~/.ansible-navigator.yml` with the following minimum settings:

```yaml
---
ansible-navigator:
  ansible:
    inventories:
      - /etc/ansible/hosts
  mode: stdout
  playbook-artifact:
    enable: false
  execution-environment:
    pull-policy: missing
    image: registry.gitlab.com/redhat-cop/ansible-ssa/ee-ansible-ssa/ee-ansible-ssa
    environment-variables:
      pass:
        - AWS_ACCESS_KEY_ID
        - AWS_SECRET_ACCESS_KEY
        - AZURE_SECRET
        - AZURE_CLIENT_ID
        - AZURE_SUBSCRIPTION_ID
        - AZURE_TENANT
        - GCP_AUTH_KIND
        - GCP_SERVICE_ACCOUNT_FILE
        - GCP_PROJECT
        - OVIRT_HOSTNAME
        - OVIRT_USERNAME
        - OVIRT_PASSWORD
        - CONTROLLER_HOST
        - CONTROLLER_USERNAME
        - CONTROLLER_PASSWORD
        - CONTROLLER_VERIFY_SSL
```

Start the Playbook:

```bash
# if you store your cloud provider credentials in a file, load them into your shell
source /path/to/your/environment-file
# make sure your ssh key is loaded to get access into your cloud instance (start ssh-agent first)
eval `ssh-agent -s`
ssh-add /path/to/key
# start the playbook
ansible-navigator run rhaap-install.yml -e @/path/to/main.yml --vault-password-file /path/to/.vault -e @/path/to/vault.yml -i /etc/ansible/hosts
```

## Manual steps

There are a couple of things which are not automated yet:

- create a namespace on Automation Hub called `ansible_ssa`
- import the latest [Ansible SSA collection](https://gitlab.com/redhat-cop/ansible-ssa/ansible-ssa-collection) into this namespace
- import the [requirements file](https://gitlab.com/redhat-cop/ansible-ssa/ee-ansible-ssa/-/raw/main/requirements-galaxy.yml) for Ansible Galaxy and start a sync
- create an API token on [console.redhat.com](console.redhat.com) and import it
- make sure the following collection on Red Hat Automation Hub are enabled for sync:
  - ansible.controller
  - amazon.aws
  - community.aws
  - community.crypto
- start the sync from public Automation Hub

We're working on getting these steps automated as well, so eventually the tasks above will not be needed.
